package com.issuefinder.crawling.dao.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class KoscomStockBasic {


    private String trdDd;
    private String mktEndTm;
    private List<StockBasic> isuLists;

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class StockBasic  {
        public String isuSrtCd;
        public String isuCd;
        public String isuKorNm;
        public String isuKorAbbr;
        public String map_to;
        public String market;
   
    }


}

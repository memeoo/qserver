package com.issuefinder.crawling.dao.api;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import javax.net.ssl.HttpsURLConnection;
import com.issuefinder.crawling.model.SMSData;
import com.issuefinder.crawling.model.SMSDataMunzaContry;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.client.RestTemplate;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Service
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class EtcApi {

    public void sendAuthNumberViaBburio(SMSData data) {
        String input = null;
        StringBuffer result = new StringBuffer();
        URL url = null;
        try {

            url = new URL("https://api.bizppurio.com/v2/message");
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.addRequestProperty("Content-Type", "application/json");
            connection.addRequestProperty("Accept-Charset", "UTF-8");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setConnectTimeout(15000);
            /** Request **/
            OutputStream os = connection.getOutputStream();
            String sms = "{\"account\":\"" + data.account + "\",\"refkey\":\"" + data.refkey + "\"," + "\"type\":\""
                    + data.type + "\",\"from\":\"" + data.from + "\",\"to\":\"" + data.to + "\","
                    + "\"content\":{\"sms\":{\"message\":\"" + data.message + "\"}}}";
            os.write(sms.getBytes("UTF-8"));
            os.flush();
            /** Response **/
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            while ((input = in.readLine()) != null) {
                result.append(input);
            }
            connection.disconnect();
            System.out.println("Response : " + result.toString());
        } catch (Exception e) {
            System.out.println(" Exception: " + e.toString());
        }
    };

    public void sendMsgViaMunzanara(SMSDataMunzaContry data) {
        String euckrURL;
        try {
            euckrURL = "http://211.233.20.184/MSG/send/web_admin_send.htm?userid=" + data.userid + "&passwd="
                    + data.passwd + "&sender=" + data.sender + "&receiver=" + data.receiver + "&encode=1&message="
                    + URLEncoder.encode(data.message, "EUC-KR");
        } catch (UnsupportedEncodingException e) {
            euckrURL= "";
            e.printStackTrace();
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders(); 
        MediaType mediaType = new MediaType("application", "x-www-form-urlencoded", Charset.forName("EUC-KR"));
        headers.setContentType(mediaType);        
        restTemplate.getForEntity(euckrURL, String.class);
        System.out.println("url => " + euckrURL);
    }
    
}
    
    

package com.issuefinder.crawling.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SMSData {
    public String account;
    public String refkey;
    public String type;
    public String from;
    public String to;
    public String message;
    
}
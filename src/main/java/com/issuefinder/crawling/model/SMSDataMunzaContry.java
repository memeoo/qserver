package com.issuefinder.crawling.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SMSDataMunzaContry {
    public String userid;
    public String passwd;
    public String sender;
    public String receiver;
    public String message;
    public String sender_name;
    public String receiver_name;
    public String return_url;
}
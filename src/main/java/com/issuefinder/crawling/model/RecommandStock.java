package com.issuefinder.crawling.model;

import lombok.*;

@Getter
@Setter
@ToString
public class RecommandStock {
    private String companyCode;
    private String collectDay;
    private Integer score;

}
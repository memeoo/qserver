package com.issuefinder.crawling.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class QUser {

    @Size(min = 4, max = 255, message = "Minimum username length: 4 characters")
    
    private String phone;
    private String name;
    private String replyNum;
    
    // private String birthday;
    // private String phone;
    // private String password;

}

package com.issuefinder.crawling.batch;

import com.issuefinder.crawling.controller.req.CrawlerRequest;
import com.issuefinder.crawling.model.vo.ResourceType;
import com.issuefinder.crawling.service.CrawlerService;
import com.issuefinder.crawling.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EventSchduler {

    @Autowired
    private CrawlerService crawlerService;

    @Autowired
    private StockService stockService;

    // UTC 시간대 기준 13시 30분 -> 한국 시간 22:30 (9시간 차이)
    @Scheduled(cron = "0 30 13 * * *")
    public void batchSise() {
        crawlerService.saveAll(CrawlerRequest.builder()
                .resourceType(ResourceType.SISE)
                .limitDays(1L)
                .build());
    }
    
    // UTC 시간대 기준 13시 -> 한국 시간 22:00 (9시간 차이) 
    @Scheduled(cron = "0 00 13 * * *")
    public void batchOhlc() {
        stockService.saveOhlc();
    }

    // 매일 17시 5분(한국 시간) 
    @Scheduled(cron = "0 5 8 * * *")
    public void batchUpdateStockList() {
        stockService.saveStockList();
    }
}

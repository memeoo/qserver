package com.issuefinder.crawling.service;

import com.issuefinder.crawling.dao.api.KoscomApi;
import com.issuefinder.crawling.dao.api.KoscomCompanyInfo;
import com.issuefinder.crawling.dao.api.KoscomRealTimePrice;
import com.issuefinder.crawling.dao.api.KoscomStockBasic;
import com.issuefinder.crawling.dao.api.KoscomStockOhlc;
import com.issuefinder.crawling.exception.ResourceNotFoundException;
import com.issuefinder.crawling.mapper.StockCompanyMapper;
import com.issuefinder.crawling.mapper.StockPriceMapper;
import com.issuefinder.crawling.model.RecommandStock;
import com.issuefinder.crawling.model.StockCompany;
import com.issuefinder.crawling.model.StockOhlc;
import com.issuefinder.crawling.model.StockPrice;
import com.issuefinder.crawling.model.StockRealAnalysis;
import com.issuefinder.crawling.model.vo.MarketType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StockService {

    private final StockCompanyMapper companyMapper;
    private final StockPriceMapper priceMapper;
    private final KoscomApi koscomApi;

    public StockCompany getStockCompany(String companyCode) {
        StockCompany stocks = companyMapper.findCompanyInfo(companyCode);
        if (ObjectUtils.isEmpty(stocks)) {
            throw new ResourceNotFoundException(companyCode);
        }
        return stocks;
    }

    public StockRealAnalysis getStockPrice(String companyCode, String startDate, String endDate) {
        List<StockPrice> stockprice = priceMapper.findPrice(companyCode, startDate, endDate);
        StockCompany stocks = companyMapper.findCompanyInfo(companyCode);

        int score = priceMapper.findScore(companyCode, startDate, endDate);
        StockRealAnalysis real = new StockRealAnalysis();
        int tradePrcie = getRealTimePrice(companyCode);

        real.setCompanyName(stocks.getCompanyName());
        real.setCompanyCode(stocks.getCompanyCode());
        real.setTradePrice(tradePrcie);
        real.setScore(score);
        real.setPriceList(stockprice);

        return real;
    }

    public List<RecommandStock> getUpperNStocks(String limit, String today) {
        int limitNum = Integer.parseInt(limit);
        String todayf = today.substring(0,4)+"-"+today.substring(4, 6)+"-"+today.substring(6, 8);
        System.out.println("todayf ==> "+ todayf);

        List<RecommandStock> nStocks = priceMapper.getUpperNStocks(limitNum, todayf);
        System.out.println("nStocks ==> "+ nStocks);

        Collections.shuffle(nStocks);
        return nStocks;
    }
    
    public List<StockOhlc> getOhlclists(String companyCode) {
        List<StockOhlc> ohlc = priceMapper.findOhlc(companyCode);
        return ohlc;
    }

    public List<StockOhlc> getOhlclistsWithScore(String companyCode) {
        List<StockOhlc> ohlc = priceMapper.findOhlcWithScore(companyCode);
        return ohlc;
    }

    public int saveOhlc() {
        KoscomStockOhlc kosdaq = koscomApi.getOhlclists(MarketType.KOSDAQ.getName());
        for(KoscomStockOhlc.StockOhlc ohlc : kosdaq.getIsuLists()) {
            ohlc.setCollectDay(LocalDate.now().toString());
            priceMapper.saveOhlc(ohlc);
        }

        KoscomStockOhlc kospi = koscomApi.getOhlclists(MarketType.KOSPI.getName());
        for(KoscomStockOhlc.StockOhlc ohlc : kospi.getIsuLists()) {
            ohlc.setCollectDay(LocalDate.now().toString());
            priceMapper.saveOhlc(ohlc);
        }
        return 1;
    }

    // public int findOhlcWithScore() {
    //     KoscomStockOhlc kosdaq = koscomApi.getOhlclists(MarketType.KOSDAQ.getName());
    //     for(KoscomStockOhlc.StockOhlc ohlc : kosdaq.getIsuLists()) {
    //         ohlc.setCollectDay(LocalDate.now().toString());
    //         priceMapper.saveOhlc(ohlc);
    //     }

    //     KoscomStockOhlc kospi = koscomApi.getOhlclists(MarketType.KOSPI.getName());
    //     for(KoscomStockOhlc.StockOhlc ohlc : kospi.getIsuLists()) {
    //         ohlc.setCollectDay(LocalDate.now().toString());
    //         priceMapper.saveOhlc(ohlc);
    //     }
    //     return 1;
    // }

    public int saveStockList() {
        System.out.println("@1 " + MarketType.KOSPI.getName());
        System.out.println("@2 " + MarketType.KOSDAQ.getName());
        
        KoscomStockBasic stocklistKospi = koscomApi.getNewStockList(MarketType.KOSPI.getName());
       
        // List<StockOhlc> ohlc = priceMapper.findOhlc(companyCode);
        priceMapper.deleteStockList();
        for(KoscomStockBasic.StockBasic stock : stocklistKospi.getIsuLists()){
            // System.out.println("stock => " + stock.getIsuKorAbbr());
            stock.setMarket("K");
            priceMapper.saveStockList(stock); 
        }
        KoscomStockBasic stocklistKosdaq = koscomApi.getNewStockList(MarketType.KOSDAQ.getName());
        // System.out.println(" stocklistKosdaq => " + stocklistKosdaq);

        for(KoscomStockBasic.StockBasic stock : stocklistKosdaq.getIsuLists()){
            stock.setMarket("Q");
            priceMapper.saveStockList(stock);  
        }
  
        return 1;
    }

    public int saveRecommendList(String date) {
        System.out.println(" saveRecommendList => " + date);
        String fomattedDate = date.substring(0,4)+"-"+date.substring(4, 6)+"-"+date.substring(6, 8);

        priceMapper.saveRecommendList(fomattedDate);
        
        KoscomStockBasic stocklistKospi = koscomApi.getNewStockList(MarketType.KOSPI.getName());
       
        // List<StockOhlc> ohlc = priceMapper.findOhlc(companyCode);
        priceMapper.deleteStockList();
        for(KoscomStockBasic.StockBasic stock : stocklistKospi.getIsuLists()){
            // System.out.println("stock => " + stock.getIsuKorAbbr());
            stock.setMarket("K");
            priceMapper.saveStockList(stock); 
        }
        KoscomStockBasic stocklistKosdaq = koscomApi.getNewStockList(MarketType.KOSDAQ.getName());
        // System.out.println(" stocklistKosdaq => " + stocklistKosdaq);

        for(KoscomStockBasic.StockBasic stock : stocklistKosdaq.getIsuLists()){
            stock.setMarket("Q");
            priceMapper.saveStockList(stock);  
        }
  
        return 1;  
    }

    public int getRealTimePrice(String companyCode) {
        StockCompany company = companyMapper.findCompanyInfo(companyCode);
        StockCompany companyOther = companyMapper.findCompanyOther(company.getMarketType());
        String companys = company.getCompanyCode() + "," + companyOther.getCompanyCode();

        KoscomRealTimePrice realTimePrice = koscomApi.getRealTimePrice(companys, company.getMarketType().toLowerCase());
        for (KoscomRealTimePrice.RealPrice realPrice : realTimePrice.getIsulist()) {
            if(realPrice.getIsuSrtCd().equals(company.getCompanyCode())) {
                return Integer.valueOf(realPrice.getTrdPrc());
            }
        }
        return 0;
    }

    public List<StockCompany> getAll() {
        return companyMapper.findCompanyList();
    }

    public void updateStock(String market) {
        List<KoscomCompanyInfo> kosdaq = koscomApi.getStockList(market);
        for(KoscomCompanyInfo companyInfo : kosdaq ) {
            companyInfo.setMarket(market.toUpperCase());
            companyMapper.save(companyInfo);
        }
    }
}

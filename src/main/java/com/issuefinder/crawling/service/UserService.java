package com.issuefinder.crawling.service;

import com.issuefinder.crawling.config.security.JwtTokenProvider;
import com.issuefinder.crawling.dao.api.EtcApi;
import com.issuefinder.crawling.exception.CustomException;
import com.issuefinder.crawling.mapper.UserMapper;
import com.issuefinder.crawling.model.QUser;
import com.issuefinder.crawling.model.SMSData;
import com.issuefinder.crawling.model.SMSDataMunzaContry;
import com.issuefinder.crawling.model.User;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;

import javax.servlet.http.HttpServletRequest;

import static com.twilio.example.Example.ACCOUNT_SID;
import static com.twilio.example.Example.PHONE_NUMBER;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserMapper userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private EtcApi etcApi;
    // private SMSData smsData;

    public String signin(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
            return jwtTokenProvider.createToken(email);
        } catch (AuthenticationException e) {
            throw new CustomException("Invalid email/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public String signup(User user) {
        if (!userRepository.existsByEmail(user.getEmail())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
            return jwtTokenProvider.createToken(user.getEmail());
        } else {
            throw new CustomException("email is already in use", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public QUser getQmember(String phone){
        QUser user = userRepository.getQUser(phone);
        System.out.println(" user => "+ user.toString());
        return user;
    }

    public String QSignup(QUser user) {
        // 5자리 랜덤 숫자 형성 
        int n = (int)Math.floor( Math.random() * 100000 + 1 );
        NumberFormat formatter = new DecimalFormat("00000");
        String number = formatter.format(n);
        System.out.println("Number with lading zeros: " + number);
        user.setReplyNum(number);
        System.out.println("User to Save : " + user);
        
        // 뿌리오 비즈 - 문자 전송 API 로직 (젓 같아서 안쓰기로 함)=======================
        System.out.println("뿌리오 문자 전송 나가신다~~~~~~ !!!!");

        etcApi = new EtcApi();
        SMSData smsData = new SMSData();
        smsData.account = "sp2421hw";
        smsData.refkey = "auth01";
        smsData.type="sms";
        smsData.from = "07048012356";
        smsData.to = user.getPhone();
        // smsData.message = "금일 추천주 /n 대우건설 /n 이슈지수 변동폭: 32";
        smsData.message = "퀀트리포트(주) 인증번호: "+user.getReplyNum();
        System.out.println("smsData => "+ smsData);
        System.out.println("etcApi => "+ etcApi);
        etcApi.sendAuthNumberViaBburio(smsData);
        userRepository.signQuser(user);
        
        // ===========================================================================
   
        // System.out.println("문자 나라 전송 나가신다~~~~~~ !!!!");
        // etcApi = new EtcApi();
        // SMSDataMunzaContry smsData = new SMSDataMunzaContry();
        
        // smsData.message="인증번호 " + user.getReplyNum();
        // smsData.userid="memeoo99";
        // smsData.passwd="shsbsy7077";
        // smsData.sender="07048012356";
        // // smsData.sender="01079993081"; 
        // smsData.receiver=user.getPhone();  

        // etcApi.sendMsgViaMunzanara(smsData);
        // userRepository.signQuser(user);

        return number;
    }

    public List<QUser> getAllQUsers(){
        List<QUser> userList = null;
        
        return userList;
    }

    public User search(String email) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
        }
        return user;
    }

    public void confirmQUser(String phone) {
        userRepository.confirmQUser(phone);
    }

    public Boolean exist(String email) {
        return userRepository.existsByEmail(email);
    }

    public User whoami(HttpServletRequest req) {
        return userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
    }

    public String refresh(String username) {
        return jwtTokenProvider.createToken(username);
    }


    public void authsms() {
        Twilio.init("AC69ad7312f80cc97aa8e5454fe2b28896", "1e02e51e1e9f9379e76d6b876ae0468c");
        Message message = Message.creator(
                new PhoneNumber("+821022234800"),
                new PhoneNumber("+82102223"),
                "Sample Twilio SMS using Java")
                .create();

        System.out.println(message);
    }
}

